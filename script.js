function Speaker(givenName, surname, email, biography){
    this.givenName = givenName;
    this.surname = surname;
    this.email = email;
    this.biography = biography;
    this.isActive = true;
}

Speaker.prototype = {
    constructor : Speaker,
    getBiography: function getBiography(){
        console.log('My name is '+this.givenName+' '+this.surname+' and '+'my email address is  '+this.email+'.')
    } ,
    markInactive: function markInactive(date){
       return !this.isActive;
    }
}
const subash = new Speaker('Subash', 'Karki', 'karki011@gmail.com', 1988);

console.log(subash.markInactive(1988));
